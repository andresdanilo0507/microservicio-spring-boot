package com.reporte.reporteventas.repository;

import com.reporte.reporteventas.models.Venta;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface VentaRepository extends CrudRepository<Venta, Long> {

    @Query(
        value = "SELECT venta_pago.id, venta_pago.created_at, venta_pago.total, usuario_ecommerce.nombre as cliente,usuario_ecommerce.email as email, usuario_ecommerce.foto as foto  FROM venta_pago  LEFT JOIN usuario_ecommerce ON  usuario_ecommerce.id =  venta_pago.usuario_ecommerce_id  WHERE exists (SELECT * FROM pedido WHERE venta_pago.id = pedido.venta_pago_id and exists (SELECT * FROM tienda_artesano_producto_pivot WHERE pedido.tienda_artesano_producto_id = tienda_artesano_producto_pivot.id and exists (SELECT * FROM tienda_artesano WHERE tienda_artesano_producto_pivot.tienda_artesano_id = tienda_artesano.id and artesano_id = :artesanoId and tienda_artesano.deleted_at is null) and tienda_artesano_producto_pivot.deleted_at is null) and pedido.deleted_at is null) and venta_pago.created_at between :fechaInicio and :fechaFin and venta_pago.deleted_at is null",
        nativeQuery = true
    )
    Iterable<Venta> reporte(@Param("artesanoId") Integer artesanoId,@Param("fechaInicio") String fechaInicio, @Param("fechaFin") String fechaFin);

}
