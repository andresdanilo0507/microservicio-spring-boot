package com.reporte.reporteventas.repository;

import com.reporte.reporteventas.models.Persona;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PersonaRepository extends CrudRepository<Persona, Integer> {

    Iterable<Persona> findByIdIn(List<Integer> ids);

    Iterable<Persona> findByDeletedAtIsNull();
}
