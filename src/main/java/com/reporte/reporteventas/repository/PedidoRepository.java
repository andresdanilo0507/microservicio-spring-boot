package com.reporte.reporteventas.repository;

import com.reporte.reporteventas.models.Pedido;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface PedidoRepository extends CrudRepository<Pedido, Long> {

    @Query(
        value = "SELECT p.id, p.cantidad, p.precio, p.subtotal, p.costo_envio, pro.nombre AS producto, categoria.nombre AS categoria  from pedido  AS p INNER JOIN tienda_artesano_producto_pivot AS tappivot ON p.tienda_artesano_producto_id = tappivot.id INNER JOIN producto AS pro ON tappivot.producto_id = pro.id INNER JOIN producto_categoria AS categoria ON pro.producto_categoria_id = categoria.id where p.venta_pago_id = :ventaId and p.deleted_at is null",
        nativeQuery = true
    )
    Iterable<Pedido> pedidos(@Param("ventaId") Integer ventaId);
}
