package com.reporte.reporteventas.repository;

import com.reporte.reporteventas.models.Artesano;
import org.springframework.data.repository.CrudRepository;


public interface ArtesanoRepository extends CrudRepository<Artesano, Integer> {

    Iterable<Artesano> findByDeletedAtIsNull();
}
