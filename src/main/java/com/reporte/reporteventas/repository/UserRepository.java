package com.reporte.reporteventas.repository;

import com.reporte.reporteventas.models.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Integer> {

    Iterable<User> findByIdIn(List<Integer> ids);
}
