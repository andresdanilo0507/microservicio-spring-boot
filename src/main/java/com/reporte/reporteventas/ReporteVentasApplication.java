package com.reporte.reporteventas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.RequestMapping;

@SpringBootApplication
@EntityScan("com.reporte.reporteventas.models")
@EnableJpaRepositories("com.reporte.reporteventas.repository")
@ComponentScan({"com.reporte.reporteventas.controller"})
public class ReporteVentasApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReporteVentasApplication.class, args);
	}

}
