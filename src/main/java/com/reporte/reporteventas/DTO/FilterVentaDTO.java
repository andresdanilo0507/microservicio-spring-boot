package com.reporte.reporteventas.DTO;

public class FilterVentaDTO {

    private Integer artesanoId;

    private String fechaInicio;

    private String fechaFin;

    public Integer getArtesanoId() {
        return artesanoId;
    }

    public void setArtesanoId(Integer artesanoId) {
        this.artesanoId = artesanoId;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }
}
