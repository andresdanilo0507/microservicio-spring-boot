package com.reporte.reporteventas.controller;

import com.reporte.reporteventas.models.Artesano;
import com.reporte.reporteventas.repository.ArtesanoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping(value="artesano")
public class ArtesanoController {
    @Autowired
    private ArtesanoRepository _artesanoRepository;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public @ResponseBody Iterable<Artesano> getAll() {
        return _artesanoRepository.findByDeletedAtIsNull();
    }

}
