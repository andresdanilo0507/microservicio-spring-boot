package com.reporte.reporteventas.controller;

import com.reporte.reporteventas.models.Persona;
import com.reporte.reporteventas.repository.PersonaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping(value="persona")
public class PersonaController {

    @Autowired
    private PersonaRepository _personaRepository;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public @ResponseBody Iterable<Persona> getAll() {
        return _personaRepository.findByDeletedAtIsNull();
    }
}
