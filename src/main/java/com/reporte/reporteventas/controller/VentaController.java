package com.reporte.reporteventas.controller;

import com.reporte.reporteventas.DTO.FilterVentaDTO;
import com.reporte.reporteventas.models.Pedido;
import com.reporte.reporteventas.models.Venta;
import com.reporte.reporteventas.repository.PedidoRepository;
import com.reporte.reporteventas.repository.VentaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping(value="venta")
public class VentaController {

    @Autowired
    private VentaRepository _ventaRepository;

    @Autowired
    private PedidoRepository _pedidoRepository;

    @RequestMapping(value = "/reporte", method = RequestMethod.POST)
    public @ResponseBody Iterable<Venta> report(@RequestBody FilterVentaDTO filter) {
        return _ventaRepository.reporte(filter.getArtesanoId(),filter.getFechaInicio(),filter.getFechaFin());
    }

    @GetMapping("pedidos/{venta_id}")
    public @ResponseBody Iterable<Pedido> report(@PathVariable("venta_id") Integer ventaId) {
        return _pedidoRepository.pedidos(ventaId);
    }
}
