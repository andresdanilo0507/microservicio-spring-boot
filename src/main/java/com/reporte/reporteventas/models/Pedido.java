package com.reporte.reporteventas.models;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@Getter
@Setter
@Table(name="pedido")
public class Pedido {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;

    private Float cantidad;

    private Float precio;

    private Float subtotal;

    @Column(name = "costo_envio")
    private Float costoEnvio;

    private String producto;

    private String categoria;


}
