FROM openjdk:17-jdk-slim
ENV APP_HOME=/apps
WORKDIR $APP_HOME/reporte-venta.jar
RUN mkdir -p $APP_HOME/log/
COPY $APP_HOME/reporte-venta.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","app.jar"]
